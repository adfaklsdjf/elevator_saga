{
  init: function(elevators, floors) {
    var elevator = elevators[0]; // Let's use the first elevator

    // set floor events
    for(i = 0; i < floors.length; i++) {
      floors[i].on("up_button_pressed", function(floor) {
        console.log('  up button pressed: ' + floor.level);
        elevator.goto(floor.level);
      });
      floors[i].on("down_button_pressed", function(floor) {
        console.log('down button pressed: ' + floor.level);
        elevator.goto(floor.level);
      });
    }
    //console.log(floors);

    elevator.goto = function(floor) {
      if(this.destinationQueue.indexOf(floor) == -1) {
        this.destinationQueue.push(floor);
        this.checkDestinationQueue();
        console.log(this.destinationQueue);
      }
    }

    elevator.on("floor_button_pressed", function(floorNum) {
      console.log('floor button pressed: ' + floorNum);
      this.goto(floorNum);
    });

    elevator.on("stopped_at_floor", function(floorNum) {
      console.log("stopped at floor " + floorNum);
      console.log(this.destinationQueue);
    });

    // Whenever the elevator is idle (has no more queued destinations) ...
    elevator.on("idle", function() {
      console.log('idle - destinationQueue - ' + this.destinationQueue.length);
      if(this.destinationQueue.length == 0) {
        console.log('queue empty');
        this.goToFloor(0);
      }
      else {
        console.log('idle fired with items in queue');
        this.checkDestinationQueue();
      }
    }); 
  },
  update: function(dt, elevators, floors) {
      // We normally don't need to do anything here
  }
}